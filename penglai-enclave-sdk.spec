Name:           penglai-enclave-sdk
Version:        1.0
Release:        1
Summary:        The SDK for penglai-enclave
ExclusiveArch:  riscv64
License:        MulanPSL-2.0
URL:            https://github.com/Penglai-Enclave/Penglai-Enclave-sPMP
Source0:        https://github.com/Penglai-Enclave/Penglai-Enclave-sPMP/releases/download/v1.0/penglai-enclave-sdk.tar.gz
BuildRequires:  g++ make openssl-devel kernel-devel kernel-source

%description
The Penglai Enclave SDK provides tools and libraries for developing secure applications using the Penglai Enclave technology. This SDK includes necessary headers and libraries to help developers get started.


%package devel
Summary: Development files for Penglai Enclave SDK
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The Penglai Enclave SDK development package contains header files and static libraries necessary for developing applications using the Penglai Enclave technology.

%package doc
Summary: Documentation for Penglai Enclave SDK
Group: Documentation
Requires: %{name} = %{version}-%{release}

%description doc
The Penglai Enclave SDK documentation package contains various guides and references for using the Penglai Enclave SDK.


%define debug_package %{nil}
%define penglai_sdk /opt/penglai

%prep
%autosetup -n %{name} -p1

%build
make -j%{_smp_build_ncpus}

%install

install -d %{buildroot}%{penglai_sdk}/lib
install -d %{buildroot}%{penglai_sdk}/runtime
install -d %{buildroot}%{penglai_sdk}/bin

find lib -name "*.a" -exec cp --parent {} %{buildroot}%{penglai_sdk} \;
find lib -name "*.h" -exec cp --parent {} %{buildroot}%{penglai_sdk} \;
cp -r penglai_sdk_ssl %{buildroot}%{penglai_sdk}
cp -r runtime/lib %{buildroot}%{penglai_sdk}/runtime
cp -r penglai-host/penglai-host %{buildroot}%{penglai_sdk}/bin
chmod +x %{buildroot}%{penglai_sdk}/bin/penglai-host
cp app.* %{buildroot}%{penglai_sdk}


%post
if [ ! -e /usr/bin/penglai-host ]; then
    ln -s %{penglai_sdk}/bin/penglai-host /usr/bin/penglai-host
fi

%postun
rm -f /usr/bin/penglai-host

%files
%defattr(-, root, root, -)
%{penglai_sdk}/bin/penglai-host
%{penglai_sdk}/runtime/lib/*.o
%{penglai_sdk}/runtime/lib/*.a
%{penglai_sdk}/runtime/lib/musl-gcc.specs
%{penglai_sdk}/penglai_sdk_ssl/lib64/*.a

%files devel
%defattr(-, root, root, -)
%{penglai_sdk}/lib/app/include/*.h
%{penglai_sdk}/lib/gm/include/*.h
%{penglai_sdk}/lib/gm/include/miracl/*.h
%{penglai_sdk}/lib/host/include/*.h
%{penglai_sdk}/lib/*.a
%{penglai_sdk}/app.lds
%{penglai_sdk}/app.mk
%{penglai_sdk}/penglai_sdk_ssl/include/openssl/*.h.in
%{penglai_sdk}/penglai_sdk_ssl/include/openssl/*.h
%{penglai_sdk}/penglai_sdk_ssl/include/openssl/*.H
%{penglai_sdk}/penglai_sdk_ssl/include/sgx_tsgxssl.edl
%{penglai_sdk}/penglai_sdk_ssl/include/*.h

%files doc
%defattr(-, root, root, -)
%{penglai_sdk}/penglai_sdk_ssl/docs/*

%changelog
* Mon Mar 18 2024 Jingwiw <wangjingwei@iscas.ac.cn> - 1.0-1
- Initial release of the penglai TEE sdk
